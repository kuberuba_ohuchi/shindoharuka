<?php $uri = get_template_directory_uri(); ?>
<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>
            <footer class="category-top collection">
                <div class="footer">
                    <div class="float">
                        <div class="right">
                            <div class="sns">
                                <ul class="flex">
                                    <li><a href="/collection/"><img class="js-switching sns--store" data-src="<?php echo $uri; ?>/assets/img/common/sns_store_pc.png" alt="store"></a></li>
                                    <li><a href="https://www.instagram.com/shindo_haruka/" target="_blank"><img class="js-switching sns--instagram" data-src="<?php echo $uri; ?>/assets/img/common/sns_instagram_pc.png" alt="instagram"></a></li>
                                    <li><a href="https://www.facebook.com/shindoharuka.fp/" target="_blank"><img class="js-switching sns--facebook" data-src="<?php echo $uri; ?>/assets/img/common/sns_facebook_pc.png" alt="facebook"></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="left">
                            <div class="copyright"><p>© 2018 SHINDO HARUKA.</p></div>
                        </div>
                    </div>
                </div>
            </footer>

<?php wp_footer(); ?>
