<?php
/*
Template Name: TOP
*/
?>
<?php $uri = get_template_directory_uri(); ?>
<!DOCTYPE html>
<html lang="ja">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta charset="utf-8">
        <meta name="description" content="<?php the_field('meta-description'); ?>">
        <meta name="keywords" content="<?php the_field('meta-keywords'); ?>">
        <meta name='viewport' content='width=device-width,user-scalable=no'>
        <meta name="format-detection" content="telephone=no">

        <title>SHINDO HARUKA Jewelry</title>

        <link rel="preconnect" href="//fonts.gstatic.com">
        <link rel="stylesheet" href="//fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@300;400;500;700&display=swap">
        <link rel="stylesheet" href="<?php echo $uri; ?>/assets/css/vendor/magnific-popup.css" media="all"> 
        <link rel="stylesheet" href="<?php echo $uri; ?>/assets/css/style.css" media="all">

        <script src="<?php echo $uri; ?>/assets/js/vendor/script.js"></script>
        <script src="<?php echo $uri; ?>/assets/js/plugins.js"></script>
         <script src="<?php echo $uri; ?>/assets/js/vendor/jquery.magnific-popup.min.js"></script> 
        <script src="<?php echo $uri; ?>/assets/js/app.js"></script>
    </head>
    <body class="<?php the_field('body-class'); ?>" data-key="<?php the_field('data-key'); ?>" data-dir="<?php the_field('data-dir'); ?>">
        <div id="wrapper">
            <?php get_header();?>

            <main>
                <div class="bg"></div>

                <div class="inner1024">
                    <article>
                        <section id="sec01">
                            <div class="img-txt late-in translateY"><img class="js-switching" data-src="<?php echo $uri; ?>/assets/img/home/home_sec01_txt01_pc.png" alt="描くように、歌うように。"></div>

                            <div class="banner late-in translateY">
                                <p class="spshow">NEWS</p>
<?php
$posts = get_posts(array(
'post_type' => 'post',
'posts_per_page' => 1, // 表示件数
));
?>
<?php if($posts): foreach($posts as $post): setup_postdata($post); ?>
                                <a class="hover--overlay--white" href="<?php the_permalink(); ?>"><?php 
$image = get_field('newsimg');
$size = 'thumbnail'; // (thumbnail, medium, large, full or custom size)
if( $image ) {
    $imgsrc = wp_get_attachment_image_src( $image, $size );
    echo '<img class="fit" src="'.$imgsrc[0] .'">';
}
?></a>
<?php endforeach; endif; ?>
                                <div class="allow--right"><a class="hover--alpha" href="news/">NEWS</a></div>
                            </div>
                        </section>



                        <section id="sec02">
                            <div class="banner late-in translateY">
                                <p class="spshow">ABOUT</p>
                                <a class="hover--overlay--white" href="about/"><img class="fit" src="<?php echo $uri; ?>/assets/img/home/home_banner02.jpg"></a>
                                <div class="allow--right"><a class="hover--alpha" href="about/">ABOUT</a></div>
                            </div>

                            <div class="img-txt pcshow late-in translateY"><img class="js-switching" data-src="<?php echo $uri; ?>/assets/img/home/home_sec01_txt02.png" alt="軽やかに、けれどあふれるほどの想いをこめて生み出されるジュエリーたち。　そこに息づいているのは、世界の美しさへの感動、そしてあなたという存在のすべてを祝福する心。"></div>
                        </section>



                        <section id="sec03">
                            <div class="banner late-in translateY">
                                <p class="spshow">COLLECTION</p>
                                <a class="hover--overlay--white" href="collection/"><img class="fit" src="<?php echo $uri; ?>/assets/img/home/home_banner03.jpg"></a>
                                <div class="allow--right"><a class="hover--alpha" href="collection/">COLLECTION</a></div>
                            </div>
                        </section>



                        <section id="sec04">
                            <div class="flex">
                                <div class="col pcshow late-in translateY">
                                    <div class="img-txt"><img class="js-switching" data-src="<?php echo $uri; ?>/assets/img/home/home_sec01_txt03.png" alt="歩み続けるあなたにいつも寄り添い、しなやかな自信と、前へと進む気持ちを届けます。"></div>
                                </div>
                                <div class="col late-in translateY">
                                    <div class="banner">
                                        <p class="spshow">Made To ORDER</p>
                                        <a class="hover--overlay--white" href="order/"><img class="fit" src="<?php echo $uri; ?>/assets/img/home/home_banner04.jpg"></a>
                                        <div class="allow--right"><a class="hover--alpha" href="order/">Made To ORDER</a></div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </article>
                </div>
            </main>

            <?php get_footer();?>
        </div>
    </body>
</html>