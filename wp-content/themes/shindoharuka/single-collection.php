<?php $uri = get_template_directory_uri(); ?>
<!DOCTYPE html>
<html lang="ja">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta charset="utf-8">
        <meta name="description" content="<?php the_field('meta-description'); ?>">
        <meta name="keywords" content="<?php the_field('meta-keywords'); ?>">
        <meta name='viewport' content='width=device-width,user-scalable=no'>
        <meta name="format-detection" content="telephone=no">

        <title><?php the_title(); ?>｜SHINDO HARUKA Jewelry</title>

        <link rel="preconnect" href="//fonts.gstatic.com">
        <link rel="stylesheet" href="//fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@300;400;500;700&display=swap">
        <link rel="stylesheet" href="<?php echo $uri; ?>/assets/css/vendor/magnific-popup.css" media="all"> 
        <link rel="stylesheet" href="<?php echo $uri; ?>/assets/css/style.css" media="all">

        <script src="<?php echo $uri; ?>/assets/js/vendor/script.js"></script>
        <script src="<?php echo $uri; ?>/assets/js/plugins.js"></script>
         <script src="<?php echo $uri; ?>/assets/js/vendor/jquery.magnific-popup.min.js"></script> 
        <script src="<?php echo $uri; ?>/assets/js/app.js"></script>
    </head>
    <body class="collection" data-key="00-03-00" data-dir="2">
        <div id="wrapper">
            <?php get_header();?>

<style type="text/css">

    #collection-detail{
        display: flex;
        width:100%;
        padding:50px 0 0 0;
        justify-content: space-between;
    }
    #collection-detail #collection-images{
        width:400px;
    }
    #collection-detail #collection-images ul{
        width:100%;
        display: flex;
        justify-content: space-between;
        flex-wrap:wrap;
        padding:20px 0 0 0;
    }
    #collection-detail #collection-images ul li{
        width:22%;
        line-height: 0;
        padding-bottom:20px;
        cursor: pointer;
    }
    #collection-detail #collection-txt{
        width:550px;
            font-size:14px;
            text-align:justify;text-justify:inter-ideograph;

    }
    #collection-body{
        line-height: 2em;
    }
    #collection-cart{
        padding:50px 0 0 0;
    }
    @media only screen and (max-width: 767px){
        h1{
            padding:50px 0 0 0;
            font-size:20px;
        }
        #collection-detail{
            display: block;
            padding:50px 5% 0 5%;
        }
        #collection-detail #collection-images{
            width:100%;
        }
        #collection-detail #collection-images ul{
            width:100%;
            padding:10px 0 20px 0;
        }
        #collection-detail #collection-images ul li{
            width:22%;
            line-height: 0;
            padding-bottom:15px;
        }
        #collection-detail #collection-txt{
            width:100%;
        }
        #collection-cart{
            padding:25px 0 0 0;
        }
    }
</style>
            <main>
                <div class="bg"></div>

                <div class="inner1024">
                    <article>
                        <section>
                            <div id="collection-detail">
                                <div id="collection-images">
                                    <div id="collection-image-main"><?php 
$image = get_field('詳細img01');
$size = 'full'; // (thumbnail, medium, large, full or custom size)
if( $image ) {
    $imgsrc = wp_get_attachment_image_src( $image, $size );
    echo '<img class="fit" src="'.$imgsrc[0] .'">';
}
?></div>
                                    <ul>
                                        <li><?php 
$image = get_field('詳細img01');
$size = 'full'; // (thumbnail, medium, large, full or custom size)
if( $image ) {
    $imgsrc = wp_get_attachment_image_src( $image, $size );
    echo '<img class="fit" src="'.$imgsrc[0] .'">';
}
?></li>
                                        <li><?php 
$image = get_field('詳細img02');
$size = 'full'; // (thumbnail, medium, large, full or custom size)
if( $image ) {
    $imgsrc = wp_get_attachment_image_src( $image, $size );
    echo '<img class="fit" src="'.$imgsrc[0] .'">';
}
?></li>
                                        <li><?php 
$image = get_field('詳細img03');
$size = 'full'; // (thumbnail, medium, large, full or custom size)
if( $image ) {
    $imgsrc = wp_get_attachment_image_src( $image, $size );
    echo '<img class="fit" src="'.$imgsrc[0] .'">';
}
?></li>
                                        <li><?php 
$image = get_field('詳細img04');
$size = 'full'; // (thumbnail, medium, large, full or custom size)
if( $image ) {
    $imgsrc = wp_get_attachment_image_src( $image, $size );
    echo '<img class="fit" src="'.$imgsrc[0] .'">';
}
?></li>
                                        <li><?php 
$image = get_field('詳細img05');
$size = 'full'; // (thumbnail, medium, large, full or custom size)
if( $image ) {
    $imgsrc = wp_get_attachment_image_src( $image, $size );
    echo '<img class="fit" src="'.$imgsrc[0] .'">';
}
?></li>
                                        <li><?php 
$image = get_field('詳細img06');
$size = 'full'; // (thumbnail, medium, large, full or custom size)
if( $image ) {
    $imgsrc = wp_get_attachment_image_src( $image, $size );
    echo '<img class="fit" src="'.$imgsrc[0] .'">';
}
?></li>
                                        <li><?php 
$image = get_field('詳細img07');
$size = 'full'; // (thumbnail, medium, large, full or custom size)
if( $image ) {
    $imgsrc = wp_get_attachment_image_src( $image, $size );
    echo '<img class="fit" src="'.$imgsrc[0] .'">';
}
?></li>
                                        <li><?php 
$image = get_field('詳細img08');
$size = 'full'; // (thumbnail, medium, large, full or custom size)
if( $image ) {
    $imgsrc = wp_get_attachment_image_src( $image, $size );
    echo '<img class="fit" src="'.$imgsrc[0] .'">';
}
?></li>
                                    </ul>
                                </div>
                                <div id="collection-txt">
                                    <div id="collection-body"><?php the_field('商品紹介'); ?></div>
                                    <div id="collection-cart"><?php the_field('cart'); ?></div>
                                </div>
                            </div>

                            <div class="page-back">
                                <div class="allow--left"><a onclick="window.history.back(); return false;">＜ Back ＞</a></div>
                            </div>
                        </section>
                    </article>
                </div>
            </main>

            <?php get_footer();?>
        </div>

<script type="text/javascript">
    $('#collection-images ul li img').click(function(){
        imgNm = $(this).attr('src');
        //chngID = $(this).parent().parent().parent().parent().attr('id');
        $('#collection-image-main img').attr('src',imgNm);
    });
</script>
    </body>
</html>