<?php $uri = get_template_directory_uri(); ?>
<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
            <header>
                <div class="inside">
                    <div class="flex">
                        <div class="col"><a href="/"><img class="js-switching head--logo" data-src="<?php echo $uri; ?>/assets/img/common/logo_header_pc.png" alt="SHINDO HARUKA Jewelry"></a></div>
                        <div class="col"><a id="menu-btn"><img class="js-switching head--menu" data-src="<?php echo $uri; ?>/assets/img/common/btn_header_menu_pc.png" alt="MENU"></a></div>
                    </div>
                </div>
            </header>

            <div id="menu">
                <div id="menu-overlay"></div>
                <a id="menu-btn--close"><span class="icon"></span></a>
                <div id="menu-list">
                    <div class="inside">
                        <div class="scroll-body">
                            <div class="upper">
                                <ul>
                                    <li><a href="/">TOP</a></li>
                                    <li><a href="/news/">News</a></li>
                                    <li><a href="/about/">About</a></li>
                                    <li><a href="/collection/">Collection</a></li>
                                    <li><a href="/order/">Made to Order</a></li>
                                    <li><a href="/project/">Project</a></li>
                                    <li><a href="/note/">Note</a></li>
                                    <li><a href="/contact/">Contact</a></li>
                                </ul>
                            </div>
                            <div class="lower spshow">
                                <div class="footer">
                                    <div class="float">
                                        <div class="right">
                                            <div class="sns">
                                                <ul class="flex">
                                                    <li><a href="/collection/"><img class="js-switching sns--store" data-src="<?php echo $uri; ?>/assets/img/common/sns_store_pc.png" alt="store"></a></li>
                                                    <li><a href="https://www.instagram.com/shindo_haruka/"><img class="js-switching sns--instagram" data-src="<?php echo $uri; ?>/assets/img/common/sns_instagram_pc.png" alt="instagram"></a></li>
                                                    <li><a href="https://www.facebook.com/shindoharuka.fp/"><img class="js-switching sns--facebook" data-src="<?php echo $uri; ?>/assets/img/common/sns_facebook_pc.png" alt="facebook"></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="left">
                                            <div class="copyright"><p>© 2018 SHINDO HARUKA.</p></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>