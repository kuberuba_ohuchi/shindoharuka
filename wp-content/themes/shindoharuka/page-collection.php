<?php
/*
Template Name: COLLECTION
*/
?>
<?php $uri = get_template_directory_uri(); ?>
<!DOCTYPE html>
<html lang="ja">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta charset="utf-8">
        <meta name="description" content="<?php the_field('meta-description'); ?>">
        <meta name="keywords" content="<?php the_field('meta-keywords'); ?>">
        <meta name='viewport' content='width=device-width,user-scalable=no'>
        <meta name="format-detection" content="telephone=no">

        <title><?php the_field('meta-title'); ?>｜SHINDO HARUKA Jewelry</title>

        <link rel="preconnect" href="//fonts.gstatic.com">
        <link rel="stylesheet" href="//fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@300;400;500;700&display=swap">
        <link rel="stylesheet" href="<?php echo $uri; ?>/assets/css/vendor/slick.css" media="all">  <link rel="stylesheet" href="<?php echo $uri; ?>/assets/css/vendor/slick-theme.css" media="all"> 
        <link rel="stylesheet" href="<?php echo $uri; ?>/assets/css/style.css" media="all">

        <script src="<?php echo $uri; ?>/assets/js/vendor/script.js"></script>
        <script src="<?php echo $uri; ?>/assets/js/plugins.js"></script>
         <script src="<?php echo $uri; ?>/assets/js/vendor/slick.min.js"></script> 
        <script src="<?php echo $uri; ?>/assets/js/app.js"></script>
    </head>
    <body class="category-top collection" data-key="00-03-00" data-dir="1">
        <div id="wrapper">
            <?php get_header();?>

            <main>
                <div class="bg"></div>

                <div class="inner1024">
                    <article>
                        <section id="newin">
                            <div class="sec-ttl"><img src="<?php echo $uri; ?>/assets/img/collection/top_ttl01.png" alt="New in"></div>

                            <div class="blocks">
<?php
$posts = get_posts(array(
'post_type' => 'collection',
'posts_per_page' => 1, // 表示件数
'taxonomy' => 'collection_cat',
'term' => 'new-in',
));
?>
<?php if($posts): foreach($posts as $post): setup_postdata($post); ?>
                                <div id="newin-01" class="block">
                                    <div class="pcshow">
                                        <div class="item--message">
                                            <p>
                                                <?php the_field('index用テキスト'); ?>
                                            </p>
                                        </div>
                                        <div class="item--left"><a class="hover--overlay--white" href="<?php the_permalink(); ?>"><?php 
$image = get_field('index用img02');
$size = 'thumbnail'; // (thumbnail, medium, large, full or custom size)
if( $image ) {
    $imgsrc = wp_get_attachment_image_src( $image, $size );
    echo '<img class="fit" src="'.$imgsrc[0] .'">';
}
?></a></div>
                                        <div class="item--center">
                                            <div class="item--ttl"><p><?php the_title(); ?></p></div>
                                            <a class="hover--overlay--white" href="<?php the_permalink(); ?>"><?php 
$image = get_field('index用img01');
$size = 'thumbnail'; // (thumbnail, medium, large, full or custom size)
if( $image ) {
    $imgsrc = wp_get_attachment_image_src( $image, $size );
    echo '<img class="fit" src="'.$imgsrc[0] .'">';
}
?></a>
                                            <div class="item--more"><div class="allow--right"><a class="hover--alpha" href="<?php the_permalink(); ?>">more</a></div></div>
                                        </div>
                                        <div class="item--right"><a class="hover--overlay--white" href="<?php the_permalink(); ?>"><?php 
$image = get_field('index用img03');
$size = 'thumbnail'; // (thumbnail, medium, large, full or custom size)
if( $image ) {
    $imgsrc = wp_get_attachment_image_src( $image, $size );
    echo '<img class="fit" src="'.$imgsrc[0] .'">';
}
?></a></div>
                                    </div>

                                    <div class="spshow">
                                        <div class="item--ttl"><p><?php the_title(); ?></p></div>
                                        <div class="item--info">
                                            <p>
                                                <?php the_field('index用テキスト'); ?>
                                            </p>
                                        </div>
                                        <div class="slickslide">
                                            <div class="slickslide001">
                                                <div class="card">
                                                    <a href="<?php the_permalink(); ?>"><?php 
$image = get_field('index用img02');
$size = 'thumbnail'; // (thumbnail, medium, large, full or custom size)
if( $image ) {
    $imgsrc = wp_get_attachment_image_src( $image, $size );
    echo '<img class="fit" src="'.$imgsrc[0] .'">';
}
?></a>
                                                </div>
                                                <div class="card">
                                                    <a href="<?php the_permalink(); ?>"><?php 
$image = get_field('index用img01');
$size = 'thumbnail'; // (thumbnail, medium, large, full or custom size)
if( $image ) {
    $imgsrc = wp_get_attachment_image_src( $image, $size );
    echo '<img class="fit" src="'.$imgsrc[0] .'">';
}
?></a>
                                                </div>
                                                <div class="card">
                                                    <a href="<?php the_permalink(); ?>"><?php 
$image = get_field('index用img03');
$size = 'thumbnail'; // (thumbnail, medium, large, full or custom size)
if( $image ) {
    $imgsrc = wp_get_attachment_image_src( $image, $size );
    echo '<img class="fit" src="'.$imgsrc[0] .'">';
}
?></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item--more"><div class="allow--right"><a href="<?php the_permalink(); ?>">more</a></div></div>
                                    </div>
                                </div>
<?php endforeach; endif; ?>
<?php
$posts = get_posts(array(
'post_type' => 'collection',
'posts_per_page' => 1, // 表示件数
'taxonomy' => 'collection_cat',
'term' => 'new-in',
'offset'=> 1
));
?>
<?php if($posts): foreach($posts as $post): setup_postdata($post); ?>
                                <div id="newin-02" class="block">
                                    <div class="pcshow">
                                        <div class="item--left"><a class="hover--overlay--white" href="<?php the_permalink(); ?>"><?php 
$image = get_field('index用img02');
$size = 'thumbnail'; // (thumbnail, medium, large, full or custom size)
if( $image ) {
    $imgsrc = wp_get_attachment_image_src( $image, $size );
    echo '<img class="fit" src="'.$imgsrc[0] .'">';
}
?></a></div>
                                        <div class="item--right">
                                            <div class="item--ttl"><p><?php the_title(); ?></p></div>
                                            <a class="hover--overlay--white" href="<?php the_permalink(); ?>"><?php 
$image = get_field('index用img01');
$size = 'thumbnail'; // (thumbnail, medium, large, full or custom size)
if( $image ) {
    $imgsrc = wp_get_attachment_image_src( $image, $size );
    echo '<img class="fit" src="'.$imgsrc[0] .'">';
}
?></a>
                                            <div class="item--more"><div class="allow--right"><a class="hover--alpha" href="<?php the_permalink(); ?>">more</a></div></div>
                                            <div class="item--info">
                                                <p>
                                                    <?php the_field('index用テキスト'); ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="spshow">
                                        <div class="item--ttl"><p><?php the_title(); ?></p></div>
                                        <div class="item--info">
                                            <p>
                                                <?php the_field('index用テキスト'); ?>
                                            </p>
                                        </div>
                                        <div class="slickslide">
                                            <div class="slickslide002">
                                                <div class="card">
                                                    <a href="<?php the_permalink(); ?>"><?php 
$image = get_field('index用img02');
$size = 'thumbnail'; // (thumbnail, medium, large, full or custom size)
if( $image ) {
    $imgsrc = wp_get_attachment_image_src( $image, $size );
    echo '<img class="fit" src="'.$imgsrc[0] .'">';
}
?></a>
                                                </div>
                                                <div class="card">
                                                    <a href="<?php the_permalink(); ?>"><?php 
$image = get_field('index用img01');
$size = 'thumbnail'; // (thumbnail, medium, large, full or custom size)
if( $image ) {
    $imgsrc = wp_get_attachment_image_src( $image, $size );
    echo '<img class="fit" src="'.$imgsrc[0] .'">';
}
?></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item--more"><div class="allow--right"><a href="<?php the_permalink(); ?>">more</a></div></div>
                                    </div>
                                </div>
<?php endforeach; endif; ?>
<?php
$posts = get_posts(array(
'post_type' => 'collection',
'posts_per_page' => 1, // 表示件数
'taxonomy' => 'collection_cat',
'term' => 'new-in',
'offset'=> 2
));
?>
<?php if($posts): foreach($posts as $post): setup_postdata($post); ?>
                                <div id="newin-03" class="block">
                                    <div class="pcshow">
                                        <div class="item--left"><a class="hover--overlay--white" href="<?php the_permalink(); ?>"><?php 
$image = get_field('index用img03');
$size = 'thumbnail'; // (thumbnail, medium, large, full or custom size)
if( $image ) {
    $imgsrc = wp_get_attachment_image_src( $image, $size );
    echo '<img class="fit" src="'.$imgsrc[0] .'">';
}
?></a></div>
                                        <div class="item--center">
                                            <div class="item--ttl"><p><?php the_title(); ?></p></div>
                                            <a class="hover--overlay--white" href="<?php the_permalink(); ?>"><?php 
$image = get_field('index用img01');
$size = 'thumbnail'; // (thumbnail, medium, large, full or custom size)
if( $image ) {
    $imgsrc = wp_get_attachment_image_src( $image, $size );
    echo '<img class="fit" src="'.$imgsrc[0] .'">';
}
?></a>
                                            <div class="item--more"><div class="allow--right"><a class="hover--alpha" href="<?php the_permalink(); ?>">more</a></div></div>
                                            <div class="item--info">
                                                <p>
                                                    <?php the_field('index用テキスト'); ?>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="item--right"><a class="hover--overlay--white" href="<?php the_permalink(); ?>"><?php 
$image = get_field('index用img02');
$size = 'thumbnail'; // (thumbnail, medium, large, full or custom size)
if( $image ) {
    $imgsrc = wp_get_attachment_image_src( $image, $size );
    echo '<img class="fit" src="'.$imgsrc[0] .'">';
}
?></a></div>
                                    </div>

                                    <div class="spshow">
                                        <div class="item--ttl"><p><?php the_title(); ?></p></div>
                                        <div class="item--info">
                                            <p>
                                                <?php the_field('index用テキスト'); ?>
                                            </p>
                                        </div>
                                        <div class="slickslide">
                                            <div class="slickslide003">
                                                <div class="card">
                                                    <a href="<?php the_permalink(); ?>"><?php 
$image = get_field('index用img03');
$size = 'thumbnail'; // (thumbnail, medium, large, full or custom size)
if( $image ) {
    $imgsrc = wp_get_attachment_image_src( $image, $size );
    echo '<img class="fit" src="'.$imgsrc[0] .'">';
}
?></a>
                                                </div>
                                                <div class="card">
                                                    <a href="<?php the_permalink(); ?>"><?php 
$image = get_field('index用img01');
$size = 'thumbnail'; // (thumbnail, medium, large, full or custom size)
if( $image ) {
    $imgsrc = wp_get_attachment_image_src( $image, $size );
    echo '<img class="fit" src="'.$imgsrc[0] .'">';
}
?></a>
                                                </div>
                                                <div class="card">
                                                    <a href="<?php the_permalink(); ?>"><?php 
$image = get_field('index用img02');
$size = 'thumbnail'; // (thumbnail, medium, large, full or custom size)
if( $image ) {
    $imgsrc = wp_get_attachment_image_src( $image, $size );
    echo '<img class="fit" src="'.$imgsrc[0] .'">';
}
?></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item--more"><div class="allow--right"><a href="<?php the_permalink(); ?>">more</a></div></div>
                                    </div>
                                </div>
                            </div>
<?php endforeach; endif; ?>

                            <div class="page-back">
                                <div class="allow--left"><a onclick="window.history.back(); return false;">＜ Back ＞</a></div>
                            </div>
                        </section>



<?php
$posts = get_posts(array(
'post_type' => 'collection',
'posts_per_page' => 999, // 表示件数
'taxonomy' => 'collection_cat',
'term' => 'and-more',
));
?>
<?php if($posts):  ?>
                        <section id="andmore">
                            <div class="sec-ttl"><img src="<?php echo $uri; ?>/assets/img/collection/top_ttl02.png" alt="and more"></div>

                            <div class="list">
                                <ul class="flex">
<?php foreach($posts as $post): setup_postdata($post); ?>
                                    <li>
                                        <div class="spshow"><p class="itme-ttl"><?php the_title(); ?></p></div>
                                        <div class="pic"><a class="hover--overlay--white" href="<?php the_permalink(); ?>"><?php 
$image = get_field('index用img01');
$size = 'thumbnail'; // (thumbnail, medium, large, full or custom size)
if( $image ) {
    $imgsrc = wp_get_attachment_image_src( $image, $size );
    echo '<img class="fit" src="'.$imgsrc[0] .'">';
}
?></a></div>
                                        <div class="pcshow">
                                            <p class="itme-ttl">
                                                <?php the_title(); ?>
                                            </p>
                                            <p class="item-txt">
                                                <?php the_field('index用テキスト'); ?>
                                            </p>
                                        </div>
                                    </li>
<?php endforeach; ?>
                                </ul>
                            </div>

                            <div class="page-back">
                                <div class="allow--left"><a onclick="window.history.back(); return false;">＜ Back ＞</a></div>
                            </div>
<?php endif; ?>
                        </section>
                    </article>
                </div>
            </main>

            <?php get_footer();?>
        </div>
    </body>
</html>