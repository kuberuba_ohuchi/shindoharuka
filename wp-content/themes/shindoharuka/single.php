<?php $uri = get_template_directory_uri(); ?>
<!DOCTYPE html>
<html lang="ja">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta charset="utf-8">
        <meta name="description" content="<?php the_field('meta-description'); ?>">
        <meta name="keywords" content="<?php the_field('meta-keywords'); ?>">
        <meta name='viewport' content='width=device-width,user-scalable=no'>
        <meta name="format-detection" content="telephone=no">

        <title><?php the_title(); ?>｜SHINDO HARUKA Jewelry</title>

        <link rel="preconnect" href="//fonts.gstatic.com">
        <link rel="stylesheet" href="//fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@300;400;500;700&display=swap">
        <link rel="stylesheet" href="<?php echo $uri; ?>/assets/css/vendor/magnific-popup.css" media="all"> 
        <link rel="stylesheet" href="<?php echo $uri; ?>/assets/css/style.css" media="all">

        <script src="<?php echo $uri; ?>/assets/js/vendor/script.js"></script>
        <script src="<?php echo $uri; ?>/assets/js/plugins.js"></script>
         <script src="<?php echo $uri; ?>/assets/js/vendor/jquery.magnific-popup.min.js"></script> 
        <script src="<?php echo $uri; ?>/assets/js/app.js"></script>
    </head>
    <body class="category-child news" data-key="00-01-01" data-dir="1">
        <div id="wrapper">
        <?php get_header();?>
            <main>
                <div class="inner1024">
                    <article>
                        <section>
                            <div class="txtbody">
                                <h1><img src="<?php echo $uri; ?>/assets/img/news/page_ttl.png" alt="News"></h1>

                                <div class="mv"><?php 
$image = get_field('newsimg');
$size = 'thumbnail'; // (thumbnail, medium, large, full or custom size)
if( $image ) {
    $imgsrc = wp_get_attachment_image_src( $image, $size );
    echo '<img class="fit" src="'.$imgsrc[0] .'">';
}
?></div>

                                <div class="txt-set">
                                    <p class="date"><?php the_time('Y.m.d'); ?></p>

                                    <h2><?php the_title(); ?></h2>

                                    <p>
                                        <?php the_content(); ?>
                                    </p>
                                </div>
                            </div>



                            <div class="page-back">
                                <div class="allow--left"><a class="hover--alpha" onclick="window.history.back(); return false;">＜ Back ＞</a></div>
                            </div>
                        </section>
                    </article>
                </div>
            </main>

            <?php get_footer();?>
        </div>
    </body>
</html>