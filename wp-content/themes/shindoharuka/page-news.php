<?php
/*
Template Name: news
*/
?>
<?php $uri = get_template_directory_uri(); ?>
<!DOCTYPE html>
<html lang="ja">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta charset="utf-8">
        <meta name="description" content="<?php the_field('meta-description'); ?>">
        <meta name="keywords" content="<?php the_field('meta-keywords'); ?>">
        <meta name='viewport' content='width=device-width,user-scalable=no'>
        <meta name="format-detection" content="telephone=no">

        <title><?php the_field('meta-title'); ?>｜SHINDO HARUKA Jewelry</title>

        <link rel="preconnect" href="//fonts.gstatic.com">
        <link rel="stylesheet" href="//fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@300;400;500;700&display=swap">
        <link rel="stylesheet" href="<?php echo $uri; ?>/assets/css/vendor/magnific-popup.css" media="all"> 
        <link rel="stylesheet" href="<?php echo $uri; ?>/assets/css/style.css" media="all">

        <script src="<?php echo $uri; ?>/assets/js/vendor/script.js"></script>
        <script src="<?php echo $uri; ?>/assets/js/plugins.js"></script>
         <script src="<?php echo $uri; ?>/assets/js/vendor/jquery.magnific-popup.min.js"></script> 
        <script src="<?php echo $uri; ?>/assets/js/app.js"></script>
    </head>
    <body class="<?php the_field('body-class'); ?>" data-key="<?php the_field('data-key'); ?>" data-dir="<?php the_field('data-dir'); ?>">
        <div id="wrapper">
            <?php get_header();?>

            <main>
                <div class="bg"></div>

                <div class="inner1024">
                    <article>
                        <section>
                            <div class="list">
                                <ul>
<?php
$posts = get_posts(array(
'post_type' => 'post',
'posts_per_page' => 9999, // 表示件数
));
?>
<?php if($posts): foreach($posts as $post): setup_postdata($post); ?>
                                    <li class="flex">
                                        <div class="txt-set">
                                            <p class="date"><?php the_time('Y.m.d'); ?></p>
                                            <p class="ttl"><?php the_title(); ?></p>
                                            <p class="txt">
                                                <?php echo mb_substr(strip_tags($post-> post_content),0,22) . '...'; ?>
                                            </p>
                                        </div>
                                        <div class="pic">
                                            <a class="hover--overlay--white" href="<?php the_permalink(); ?>"><?php 
$image = get_field('newsimg');
$size = 'thumbnail'; // (thumbnail, medium, large, full or custom size)
if( $image ) {
    $imgsrc = wp_get_attachment_image_src( $image, $size );
    echo '<img class="fit" src="'.$imgsrc[0] .'">';
}
?></a>
                                        </div>
                                    </li>
<?php endforeach; endif; ?>
                                </ul>
                            </div>



                            <div class="page-back">
                                <div class="allow--left"><a class="hover--alpha" onclick="window.history.back(); return false;">＜ Back ＞</a></div>
                            </div>
                        </section>
                    </article>
                </div>
            </main>

            <?php get_footer();?>
        </div>
    </body>
</html>